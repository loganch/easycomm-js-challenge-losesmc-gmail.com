const server = require('./lib/server');
const port = process.env.port || 3000;

server.listen(port, ()  => {
    console.log(`Example app listening on port ${port}!`);
});
