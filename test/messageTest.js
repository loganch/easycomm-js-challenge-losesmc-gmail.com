const chai = require('chai');
const expect = chai.expect;
const Message = require('../lib/message');

describe('Message', () => {
  describe('#validations', function () {
    it('should check the format for emails', () => {
      const message = new Message({senderEmail: 'garbage text', recipientEmail: 'garbage text'});
      expect(message.validateSync().errors['senderEmail'].message).to.eq('not a valid email');
      expect(message.validateSync().errors['recipientEmail'].message).to.eq('not a valid email');
      const validMessage = new Message({senderEmail: 'garbage@text.com', recipientEmail: 'garbage@text.com'});
      expect(validMessage.validateSync()).to.be.undefined;
    });
  });

  describe('queries', function() {
    it('should return a message when searching by id', () => {
      const message = new Message({senderEmail: 'text@example.com', recipientEmail: 'text@example.com'});
      return message.save().then((m) => {
        return Message.findById(m.id).exec().then((message) => {
          expect(message).to.be.a('object');
        })
      });
    });
  });

  describe('.sender', () => {
    describe('when the email is set', () => {
      it('returns the email', () => {
        let message = new Message({senderEmail: 'example@text.com', recipientEmail: 'example@text.com'});
        // expect(message.senderEmail).to.eq('example@text.com');
        expect(message.sender).to.eq('example@text.com');
      });
    });

    describe('when the phone is set', () => {
      it('returns the phone', () => {
        let message = new Message({senderPhone: '+14155551212', recipientPhone: '+15105551212'});
        expect(message.sender).to.eq('+14155551212')
      });

      describe('and the email is set', () => {
        it('returns the phone', () => {
          let message = new Message({senderPhone: '+14155551212', recipientPhone: '+15105551212', senderEmail: 'example@text.com'});
          expect(message.sender).to.eq('+14155551212')
        });
      });
    });

  });


  describe('.recipient', () => {
    describe('when the email is set', () => {
      it('returns the email', () => {
        let message = new Message({senderEmail: 'example@text.com', recipientEmail: 'example@text.com'});
        // expect(message.senderEmail).to.eq('example@text.com');
        expect(message.recipient).to.eq('example@text.com');
      });
    });

    describe('when the phone is set', () => {
      it('returns the phone', () => {
        let message = new Message({senderPhone: '+14155551212', recipientPhone: '+15105551212'});
        expect(message.recipient).to.eq('+15105551212')
      });

      describe('and the email is set', () => {
        it('returns the phone', () => {
          let message = new Message({senderPhone: '+14155551212', recipientPhone: '+15105551212', recipientEmail: 'example@text.com'});
          expect(message.recipient).to.eq('+15105551212')
        });
      });
    });

  });

  describe('.secureId', () => {
    it('is created when the message is saved', function() {
      let message = new Message({senderEmail: 'example@text.com', recipientEmail: 'example@text.com', body: 'simple message'});
      return message.save().then(() => {
        expect(message.secureId).not.to.be.undefined;
      });
    });
    it('doesnt change when the message is updated', () => {
      let message = new Message({senderEmail: 'example@text.com', recipientEmail: 'example@text.com', body: 'simple message'});
      return message.save().then(() => {
        const firstSecureId = message.secureId;
        message.senderEmail = 'otherExample@text.com';
        return message.save().then(() => {
          expect(message.secureId).to.eq(firstSecureId);
        });
      });
    });
  });
});