const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const Message = require('../lib/message');
const MessageCreator = require('../lib/MessageCreator');
const Promise = require('bluebird');
require('sinon-as-promised')(Promise);

describe('MessageCreator', () => {
  let creator;
  beforeEach(() =>{
    creator = new MessageCreator({
      senderEmail: "bob@example.com",
      recipientEmail: "john@johndoe.com",
      body: "Hey John, it's me Bob."
    });
  });

  describe('constructor', () => {
    it("makes a Message", () => {
      expect(creator.message).not.to.be.undefined;
      expect(creator.message.senderEmail).to.eq("bob@example.com");
    });
  });

  describe('.deliver', () => {
    it("saves the message", () => {
      const spy = sinon.spy(creator.message, 'save');
      creator.deliver();
      expect(spy.calledOnce).to.be.true;
    });

    context('when the recipientEmail is set', () => {
      it("sends an email message", () => {
        const stub = sinon.stub(creator.emailTransport, 'sendMail').resolves({response:'ZZZ Test Message sent'});
        creator.message.secureId = "123xyz";
        creator.host = "localhost:3000";

        const mailOpts = {
          from: 'bob@example.com',
          to: 'john@johndoe.com',
          subject: "You have a secure message",
          text: "Go here to view your message: http://localhost:3000/api/messages/123xyz"
        };
        return creator.deliver().then(() => {
          expect(stub.firstCall.args[0]).to.include(mailOpts);
        });
      });

      describe("when the params are not good", () => {
        beforeEach(() => {
          creator = new MessageCreator({
            senderEmail: "bobexample.com",
            recipientEmail: "john@johndoe.com",
            body: "Hey John, it's me Bob."
          })
        });

        it("surfaces the error", () => {
          return creator.deliver().catch((err) => {
            expect(err.errors.senderEmail.message).to.be.eq("not a valid email");
          });
        });
      });

    });

    context('when the recipientPhone is set', () => {
      beforeEach(() => {
        creator = new MessageCreator({
          senderPhone: "+15005550006",
          recipientPhone: "+15105551212",
          body: "Hey John, it's me Bob."
        })
      });

      it("sends an SMS via twilio", () => {
        const twilio = require('twilio');
        const twilioClient = twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);
        creator.message.secureId = "123xyz";
        creator.host = "localhost:3000";

        const textOpts = {
          from: '+15005550006',
          to: '+15105551212',
          body: "Go here to view your message: http://localhost:3000/api/messages/123xyz"
        };

        return creator.deliver().then((e) => {
          expect(creator.messageResponse.accountSid).to.eq(process.env.TWILIO_ACCOUNT_SID)
          expect(creator.messageResponse.status).to.eq('queued')
        });
      });
    });
  });

  describe('.sendMessage', () => {
    it("should return a promise", () => {
      expect(creator.sendMessage()).to.be.a('promise');
    });
  });
});

