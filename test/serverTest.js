const chai = require('chai');
const expect = chai.expect;
const request = require('supertest');
const server = require('../lib/server');
const Message = require('../lib/message');

describe('server', () => {
  it('responds to /', () => {
    return request(server).get('/').expect(200);
  });

  describe('when passed a message secure id', () => {
    it('returns the body of the message', () => {
      let message = new Message({
        senderEmail: 'test@example.com',
        recipientEmail: 'test@example.com',
        body: 'hello there',
      });
      return message.save().then(() => {
        return request(server).get(`/messages/${message.secureId}`).then((response) => {
          expect(response.status).to.eq(200);
          expect(response.text).to.match(/hello there/);
        });

      });
    });
  });
});
