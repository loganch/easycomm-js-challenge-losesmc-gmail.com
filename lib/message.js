const randomstring = require('randomstring');
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

mongoose.connect('mongodb://localhost/easycomm');

const emailFormat = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

const validateEmail = (v) => {
  return emailFormat.test(v);
};

const messageSchema = mongoose.Schema({
  secureId: String,
  senderEmail: {
    type: String,
    validate: {
      validator: validateEmail,
      message: 'not a valid email'
    }
  },
  recipientEmail: {
    type: String,
    validate: {
      validator: validateEmail,
      message: 'not a valid email'
    }
  },
  body: String,
  senderPhone: String,
  recipientPhone: String
});

messageSchema.virtual('sender').get(function() {
  return this.senderPhone || this.senderEmail;
});

messageSchema.virtual('recipient').get(function() {
  return this.recipientPhone || this.recipientEmail;
});


messageSchema.pre('validate', function(next) {
  if (!this.secureId) {
    this.secureId = randomstring.generate();
  }
  next();
});

const Message = mongoose.model('Message', messageSchema );

module.exports = Message;
