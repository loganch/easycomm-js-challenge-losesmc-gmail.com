const express = require('express');
const app = express();
const Message = require('./message');

// respond with "hello world" when a GET request is made to the homepage
app.get('/', (req, res) =>  {
  res.send('<h1>Welcome to Easycomm</h1><p>Please consult README.md for more information about this challenge</p>')
});

app.get('/messages/:secureId', (req, res) => {
  Message.findOne({secureId: req.params.secureId}).exec().then((message) => {
    res.send(`<h1>Here's your message</h1>
                 <p>Sender: ${message.sender}</p>
                 <p>Recipient: ${message.recipient}</p>
                 <hr/>
                 <p>${message.body}</p>`);
  });
});

module.exports = app;