const P = require('bluebird');
const nodemailer = P.promisifyAll(require('nodemailer'));
const Message = require('./message');
const twilio = P.promisifyAll(require('twilio'));
const twilioClient = twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN); //set environment variables in bash using export

class MessageCreator {
  constructor(message) {
    this.message = new Message(message);
    this.emailTransport = nodemailer.createTransport({
      port: 1025,
      ignoreTLS: true,
    });
    this.host = `localhost:${process.env.port || 3000}`;
    this.twilioClient = twilioClient;
    this.messageResponse = {accountSid: 'no twilio call was made'};
  }

  deliver() {
    return this.message.save()
      .then(this.sendMessage.bind(this));
  }

  sendMessage() {
    if(this.mailOptions) {
      return this.sendEmail();
    }
  }

  sendSMS() {
    return this.twilioClient.sendMessage(this.textOptions).then(info => {
        this.messageResponse.accountSid = info.account_sid;
        this.messageResponse.status = info.status;
      }).catch(err => {
        console.log(err);
      });
  }

  sendEmail() {
    return this.emailTransport.sendMail(this.mailOptions).then(info => {
        console.log('Message sent: ' + info.response)
      });
  }

  get mailOptions() {
    if (this.message.senderEmail && this.message.recipientEmail) {
      return {
        from: this.message.senderEmail,
        to: this.message.recipientEmail,
        subject: 'You have a secure message',
        text: `Go here to view your message: http://${this.host}/api/messages/${this.message.secureId}`
      }
    } 
    else {
      return null;
    }
  }

  get textOptions() {
    if (this.message.senderPhone && this.message.recipientPhone) {
      return {
        from: this.message.senderPhone,
        to: this.message.recipientPhone,
        body: `Go here to view your message: http://${this.host}/api/messages/${this.message.secureId}`
      }
    }
    else {
      return null;
    }
  }
}

module.exports = MessageCreator;