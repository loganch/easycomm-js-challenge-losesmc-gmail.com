# Welcome

This application enables easy, private communications via an open source server. Since email and SMS are not reliably
encrypted, easycomm sends a notification via these channels that you have an encrypted message on the easycomm server.

Easycomm is not full-featured.  It lacks some basic things, such as authentication.  Your pull requests are welcome,
but the goal of easycomm is primarily to contain clear, easy-to-understand code.

# Setup

You will not need to sign up for any service. You can use `homebrew` (OS X) or `chocolatey` (Windows) for your installs.

- Install Node version 7+

- Install npm and yarn 

- Install git

- Install mongodb and start the mongo server

- Clone this repository

- `yarn install` to install necessary libraries

- In a separate tab, you'll want to run `./node_modules/maildev/bin/maildev`.  You can access the maildev interface via the browser at the port it gives you (i.e., `http://localhost:1080`).

- `mocha` to run the test suite

- `node app` to run the app server

- Check out the app at 'http://localhost:3000'

# Challenge

This application _should_ support SMS, but it doesn't seem to be working.  By entering phone numbers into the sender and
recipient fields for your message, you should be able to send a text message to your phone.

### For the challenge to be considered complete:

- The tests currently in the repository must pass

- For any new code you add, public methods must be tested

### Important information for using Twilio:

- You'll need a developer Twilio account to make this work. Do not commit your credentials to the repo.

- To get the tests to pass, you'll want to use test credentials from Twilio. Twilio's web interface is jam-packed, so here's a direct link to the test credentials: https://www.twilio.com/console/phone-numbers/dev-tools/test-credentials

# Submission

- You have been granted access to an individual bitbucket repository. If you do not complete the challenge within your allotted time, your access may expire.  Please contact us if you need additonal time.

- As you work on the challenge, commit your work to a branch other than `master`.  You can create and start work on a new branch with `git checkout -b your_branch_name`

- Submit your work by creating a pull request to the master branch of your repo.

- Within your pull request, explain your rationale for your approach to the problem.  
    - Why add those methods or objects?
    - Why modify where you modified, not elsewhere?

- Your employer contact will be notified once you've submitted the challenge.  Please give us 1 week to evaluate your work and get back to you.

# Resources

- [Bitbucket setup](https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101)
- [Twilio API docs](https://www.twilio.com/docs/api/rest/sending-messages)
- [Mongoose](http://mongoosejs.com/)
- [Express](http://expressjs.com/)
- [Mocha](https://mochajs.org/)
- [Chai](http://chaijs.com/)
